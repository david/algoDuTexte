#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "naif.h"
#include"recherche.h"



int algorithme_naif(char * texte, char * motif, int n, int m){
  int i,j;
  int res;
  for(i=0, res=0; i <= (n-m); i++){
    j = 0;
    while(j<m && comparer_lettre(texte[i+j], motif[j]))
      j++;
    if(j==m)
      res++;
  }
  return res;
}

int recherche(char * texte,char * motif,int n,int m,int k){
  return algorithme_naif(texte, motif, n, m);
}
