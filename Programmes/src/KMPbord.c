#include <stdio.h>
#include<stdlib.h>
#include<string.h>

#define MAX(x,y) (x<y)?y:x


int * KMPbord(char * mot,int m){
  int i,j;
  int * table_bord=(int *)malloc(sizeof(int)*(m+1));
  table_bord[0]=-1;
  table_bord[1]=0;
  j=0;
  for(i=2;i<=m;i++){
    while(j>=0&&mot[j]!=mot[i-1])
      j=table_bord[j];
    j++;
    if(mot[i]!=mot[j])
      table_bord[i]=j;
    else
      table_bord[i]=MAX(0,table_bord[j]);
  }
  return table_bord;
}



int main(int argc,char ** argv){
  int m,i;
  int * table_bord;
  if(argc==2){
    m=strlen(argv[1]);
    table_bord=KMPbord(argv[1],m);
    for(i=0;i<=m;i++)
      printf("%d ",table_bord[i]);
    printf("\n");
    free(table_bord);
  }
  return EXIT_SUCCESS;
}
