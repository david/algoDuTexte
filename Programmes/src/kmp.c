#include <stdio.h>
#include<stdlib.h>
#include<string.h>
#include "recherche.h"

#define MAX(x,y) (x<y)?y:x




int * kmp_bord(char * mot,int m){
  int i,j;
  int * table_bord=(int *)malloc(sizeof(int)*(m+1));
  table_bord[0]=-1;
  table_bord[1]=0;
  j=0;
  for(i=2;i<=m;i++){
    while(j>=0&&mot[j]!=mot[i-1])
      j=table_bord[j];
    j++;
    if(j==0||mot[i]!=mot[j])
      table_bord[i]=j;
    else
      table_bord[i]=table_bord[j];
  }
  return table_bord;
}


int knuth_morris_pratt(char * texte,char * motif,int n,int m){
  int i=0,j=0;
  int * table_bord;
  int occ=0;
  table_bord=kmp_bord(motif,m);
  while(i<=(n-m+j)){
    while(j<m&&comparer_lettre(texte[i],motif[j])){
      i++;
      j++;
    }
    if(j==m)
      occ++;
    if(j==0)
      i++;
    else 
      j=table_bord[j];
  }
  free(table_bord);
  return occ;
}

int recherche(char * texte,char * motif,int n,int m,int k){
  return knuth_morris_pratt(texte,motif,n,m);
}
