#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "recherche.h"
#include <sys/stat.h>


char * charger_texte(const char * path,int * n){
  FILE * f;
  struct stat st; 
  char * buffer=NULL;
  
  if((f=fopen(path,"r"))!=NULL){
    stat(path, &st);
    *n=st.st_size;
    buffer=(char *)malloc(sizeof(char)*st.st_size);
    if(fread(buffer,sizeof(char),*n,f)!=*n)
      printf("Erreur de lecture\n");
  }
  fclose(f);
  return buffer;
}


int main(int argc, char ** argv){
  int n,m,nb_occ;
  char * texte;
  char * motif;

  if(argc==3){
    texte=charger_texte(argv[1],&n);
    motif=argv[2];
    m=strlen(motif);
    
    nb_occ=recherche(texte,motif,n,m,0);
    printf("%d occurrences ont été trouvées après %d comparaisons\n",nb_occ,nb_comparaisons);	  
    free(texte);
  } 
  else
    printf("./naif [fichier_source] [motif]\n");



  return EXIT_SUCCESS;
}
