#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include "randWord.h"
#include "recherche.h"


#define NB_TEST 10000

int main(int argc, char ** argv){
  int n, m, k, i, nb_moyen;
  char * texte,*motif;
  srand(time(NULL));
  if(argc==4){
    n=atoi(argv[1]);
    m=atoi(argv[2]);
    k=atoi(argv[3]);
    printf("n=%d, m=%d et k=%d\n",n,m,k);

    texte=(char*)malloc(sizeof(int)*(n+1));
    motif=(char*)malloc(sizeof(int)*(m+1));    
    for(i=0,nb_moyen=0;i<NB_TEST;i++){
      nb_comparaisons=0;
      randWord2(texte,n,k);
      randWord2(motif,m,k);
      recherche(texte,motif,n,m,k);
      nb_moyen+=nb_comparaisons;
    }
    printf("En moyenne, le nombre de comparaisons par lettre est égal à %f\n",((double)nb_moyen)/(n*NB_TEST));
    free(texte);
    free(motif);
  
  }
  else
    printf("./naif [longueur_texte] [longueur_motif] [taille_alphabet]\n");


  return EXIT_SUCCESS;
}

