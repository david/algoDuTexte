#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include"randWord.h"

int main(int argc,char ** argv){
  int n,k;
  char * res;
  if(argc==3){
    n=atoi(argv[1]);
    k=atoi(argv[2]);   
    printf("%d %d\n",n,k);   
    srand(time(NULL));
    res=randWord(n,k);
    printf("%s\n",res);
    free(res);
  }
  else
    printf("./randWord size AlphabetSize\n");
  return EXIT_SUCCESS;
}
