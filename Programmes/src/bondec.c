#include <stdio.h>
#include<stdlib.h>
#include<string.h>

#define MAX(x,y) (x<y)?y:x

void echanger(char * x,char * y){
  char tmp=*x;
  *x=*y;
  *y=tmp;  
}


void inverser_mot(char * motif,int m){
  int i,j;
  for(i=0,j=m-1;i<j;++i,--j)
    echanger(&motif[i],&motif[j]);
} 

int * kmp_bord(char * mot,int m){
  int i,j;
  int * table_bord=(int *)malloc(sizeof(int)*(m+1));
  table_bord[0]=-1;
  table_bord[1]=0;
  j=0;
  for(i=2;i<=m;i++){
    while(j>=0&&mot[j]!=mot[i-1])
      j=table_bord[j];
    j++;
    if(mot[i]!=mot[j])
      table_bord[i]=j;
    else
      table_bord[i]=MAX(0,table_bord[j]);
  }
  return table_bord;
}

int * bm_bon_decalage(char * mot,int m){
  int * table_bord;
  int * bon_suff=(int *)malloc(sizeof(int)*(m+1));
  int i,l;
  inverser_mot(mot,m);
  table_bord=kmp_bord(mot,m);
  inverser_mot(mot,m);


  for(l=0;l<=m;l++){
    bon_suff[l]=m-(table_bord[m]);
  }

  for(i=1;i<=m;i++){
    l=m-(table_bord[i]);
    if(bon_suff[l]>(i-(table_bord[i])) ) 
      bon_suff[l]=i-(table_bord[i]);
  }
    
  return bon_suff;
}


int main(int argc,char ** argv){
  int m,i;
  int * bondec;
  if(argc==2){
    m = strlen(argv[1]);
    bondec = bm_bon_decalage(argv[1], m);
    for(i = 0; i <= m; i++)
      printf("%d ", bondec[i]);
    printf("\n");
    free(bondec);
  }
  return EXIT_SUCCESS;
}
