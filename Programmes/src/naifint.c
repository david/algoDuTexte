#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "naif.h"
#include"recherche.h"



int algorithme_naif(char * texte,char * motif,int n,int m){
  int i,j;
  int psize=m>>2;
  unsigned int * p=(unsigned int *)motif;
  unsigned int * t;
  int res;
  for(i=0,res=0;i<=(n-m);i++){
    j=0;
    t=(unsigned int *)&texte[i];
    while(j<psize&&comparer_lettres(t[j],p[j])){
      j++;
    }
    if(j==psize){
      j=m-m%4;
      while(j<m&&comparer_lettres(texte[i+j],motif[j]))
	j++;
      if(j==m)
	res++;
    }
  }
  return res;
}

int recherche(char * texte,char * motif,int n,int m,int k){
  return algorithme_naif(texte,motif,n,m);
}
