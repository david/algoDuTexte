#include <stdio.h>
#include<stdlib.h>
#include<string.h>
#include "recherche.h"


#define MAX(x,y) (x<y)?y:x



int * derniere_occ(char * mot,int m,int k){
  int * last_occ=(int *)malloc(sizeof(int)*k);
  int i;
  for(i=0;i<k;i++)
    last_occ[i]=m;
  for(i=0;i<(m-1);i++)
    last_occ[(int)(mot[i]-'a')]=m-i-1;

  for(i=0;i<k;i++) 
    printf("%d ",last_occ[i]); 
  printf("\n"); 

  return last_occ;
}

void echanger(char * x,char * y){
  char tmp=*x;
  *x=*y;
  *y=tmp;  
}

void inverser_mot(char * motif,int m){
  int i,j;
  for(i=0,j=m-1;i<j;++i,--j)
    echanger(&motif[i],&motif[j]);
} 

int * kmp_bord(char * mot,int m){
  int i,j;
  int * table_bord=(int *)malloc(sizeof(int)*(m+1));
  table_bord[0]=-1;
  table_bord[1]=0;
  j=0;
  for(i=2;i<=m;i++){
    while(j>=0&&mot[j]!=mot[i-1])
      j=table_bord[j];
    j++;
    if(mot[i]!=mot[j])
      table_bord[i]=j;
    else
      table_bord[i]=MAX(0,table_bord[j]);
  }
  return table_bord;
}

int * bm_bon_decalage(char * mot,int m){
  int * table_bord;
  int * bon_suff=(int *)malloc(sizeof(int)*(m+1));
  int i,l;
  inverser_mot(mot,m);
  table_bord=kmp_bord(mot,m);
  inverser_mot(mot,m);


  for(l=0;l<=m;l++){
    bon_suff[l]=m-(table_bord[m]);
  }

  for(i=1;i<=m;i++){
    l=m-(table_bord[i]);
    if(bon_suff[l]>(i-(table_bord[i])) ) 
      bon_suff[l]=i-(table_bord[i]);
  }
    
   for(i=0;i<=m;i++) 
     printf("%d ",bon_suff[i]); 
   printf("\n"); 

  return bon_suff;
}

int boyer_moore(char * texte, char * motif,int n,int m,int k){
  int * last_occ=derniere_occ(motif,m,k);
  int * bon_suff=bm_bon_decalage(motif,m);
  int i,j;
  int l,l2;
  int occ=0;
  i=0;
  printf("%s\n",texte);
  while(i<(n-m+1)){
    j=m-1;
    while(j>=0&&comparer_lettre(texte[i+j],motif[j]))
      j--;
    for(l=0;l<(i+j);l++)printf(" ");
    for(l2=j;l2<m;l2++)printf("%c",motif[l2]);
    printf("\n");
    if(j<0){
      occ++;
      i+=bon_suff[0];
    }
    else
      i+=MAX(bon_suff[j+1],last_occ[(int)(texte[i + j]-'a')] - m + 1 + j);
  }
  return occ;
}

int recherche(char * texte,char * motif,int n,int m,int k){
  return boyer_moore(texte,motif,n,m,k);
}

