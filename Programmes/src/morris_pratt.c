#include <stdio.h>
#include<stdlib.h>
#include<string.h>
#include"recherche.h"

#define MAX(x,y) (x<y)?y:x


int * bord(char * mot,int m){
  int i,j;
  int * table_bord=(int *)malloc(sizeof(int)*(m+1));
  table_bord[0]=-1;
  table_bord[1]=0;
  j=0;
  for(i=2;i<=m;i++){
    while(j>=0&&mot[j]!=mot[i-1])
      j=table_bord[j];
    j++;
    table_bord[i]=j;
  }
  return table_bord;
}

int morris_pratt(char * texte,char * motif,int n,int m){
  int i=0,j=0,space;
  int * table_bord;
  int occ=0;
  table_bord=bord(motif,m);
  /* Décommentez pour voir l'exécution détaillée de l'algorithme 
     printf("%s\n",texte); */
  while(i<=(n-m+j)){
    /*
    Remplacez la boucle while par ces deux lignes pour voir l'exécution détaillée de l'algorithme
    for(space=0;space<i;space++)printf(" ");
    while(j<m && printf("%c",motif[j]) && comparer_lettre(texte[i],motif[j])){ 
    */
    while(j<m && comparer_lettre(texte[i],motif[j])){ 
      i++;
      j++;
    }
    /* Décommentez pour voir l'exécution détaillée de l'algorithme 
       printf("\n"); */
    if(j==m)
      occ++;
    if(j==0)
      i++;
    else
      j=table_bord[j];
  }
  free(table_bord);
  return occ;
}

int recherche(char * texte,char * motif,int n,int m,int k){
  return morris_pratt(texte,motif,n,m);
}

